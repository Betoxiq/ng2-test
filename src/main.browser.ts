import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { NgModule, provide } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './material.module';
import { MdIconRegistry } from '@angular2-material/icon';

import { App } from './app/app';
import appModule from './app';

@NgModule({
  bootstrap: [
    App
  ],
  declarations: [
    App
  ],
  imports: [
    // Angular 2
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([], {
      useHash: true
    }),
    // app
    appModule,
    MaterialModule
    // vendors
  ],
  providers: [
    MdIconRegistry,
    provide(Window,{useValue: window} )
 ]
})
class MainModule {}

export function main() {
  return platformBrowserDynamic().bootstrapModule(MainModule);
}


// Hot Module Replacement

export function bootstrapDomReady() {
  // bootstrap after document is ready
  document.addEventListener('DOMContentLoaded', main);
}

if ('development' === ENV && HMR) {
  // activate hot module reload
  if (document.readyState === 'complete') {
    main();
  } else {
    bootstrapDomReady();
  }
  module.hot.accept();
} else {
  bootstrapDomReady();
}
