import { Component } from '@angular/core';

@Component({
styles:[`
    * {
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }
    .secondary {
      color: rgba(0, 0, 0, .54);
    }
    md-sidenav-layout {
      background: rgba(0, 0, 0, .03);
    }
    md-sidenav {
      width: 300px;
    }
`]
})
export class App {

  constructor() {
  }
}

