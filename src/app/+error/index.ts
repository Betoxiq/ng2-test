import { BrowserModule } from '@angular/platform-browser'
import { RouterModule, RouterConfig } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { NotFound } from './notfound';
import { AccessDenied } from './accessdenied';

export const ROUTER_CONFIG = [
  { path: '', component: NotFound, pathMatch: 'full' },
  { path: '404', component: NotFound  },
  { path: '403', component: AccessDenied },

];

@NgModule({
  declarations: [
    NotFound, AccessDenied
  ],
  imports: [
    // Components / Directives/ Pipes
    RouterModule.forChild(ROUTER_CONFIG),
    FormsModule,
    BrowserModule
  ]
})
export default class ErrorModule {
  static routes = ROUTER_CONFIG;
}

