import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {
    /**
     *
     */
    constructor(private router: Router) {

    }

    public isLoggedIn(): Observable<boolean> | boolean {
        let router: Router = this.router;
        let obs;
        try {

            console.log('trygin auth service');

            obs = Observable.of(true);
            // this.http.get('/api/check/logged')
            //     .map(result => result.json())
            //     .map(resultJson => (resultJson && resultJson.success));

        } catch (err) {
            obs = Observable.of(false);
        }

        return obs
            .map(success => {
                // navigate to login page
                if (!success)
                    router.navigate(['/login']);

                return success;
            });
    }
}