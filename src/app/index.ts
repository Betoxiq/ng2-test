import { RouterModule  } from '@angular/router';
import { NgModule,provide } from '@angular/core';
import {runEffects} from '@ngrx/effects';

import homeModule, { Home } from './home';
import {Login} from './home/login';
import {AuthGuard} from './authguard';
import {AuthService} from './authservice';
import {provideStore } from '@ngrx/store';
import {AssetsReducer}  from '../assets/reducer';
import {AssetActions} from '../assets/actions';
import {AssetEffects} from '../assets/effects';
import {AssetService} from '../assets/service';

const ROUTER_CONFIG = [
  { path: '', component: Home, pathMatch: 'full' },
  { path: 'login', component: Login },
  { path: 'about', loadChildren: './+about' },
  { path: 'directory', loadChildren: './+directory', canLoad: [AuthGuard] },
  { path: 'error', loadChildren: './+error' },
  { path: '**', redirectTo: '/error' },

];

@NgModule({
  providers: [
      AuthGuard,
      AuthService,
      provideStore({assets: AssetsReducer }),
      runEffects([AssetEffects]),
      AssetService,
      AssetActions
      ],
  declarations: [
    // Components / Directives/ Pipes
    Login,
    Home,
  ],
  imports: [
    RouterModule.forChild(ROUTER_CONFIG),
    homeModule
  ],
})
export default class AppModule {
  static routes = ROUTER_CONFIG
}

