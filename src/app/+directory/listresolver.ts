import {Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Store} from '@ngrx/store';
import {AppState} from '../../assets/state';
import {Asset} from '../../assets/model';


@Injectable()
export class ListResolver implements Resolve<any> {
    assets: Observable<any>;

    constructor( private store: Store<AppState> ) {
        console.log('list resolver');
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Asset[]>  {
        console.log('resolving assets');
        console.log(this.store);
        return this.store.select(state=>state.assets);
    }
}
