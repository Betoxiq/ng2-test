import { BrowserModule } from '@angular/platform-browser'
import { RouterModule, RouterConfig  } from '@angular/router';
import { NgModule } from '@angular/core';



import { List } from './list';
import { Detail } from './detail';
import {DetailResolver} from './detailresolver';
import {ListResolver} from './listresolver';

export const ROUTER_CONFIG = [
  { path: '', component: List, pathMatch: 'full',
  //resolve: { assets: ListResolver }
},
  { path: 'detail/:id', component: Detail, resolve: { detail: DetailResolver}  }
];


@NgModule({
  providers:[DetailResolver,ListResolver],
  declarations: [
    Detail,
    List
  ],
  imports: [
    // Components / Directives/ Pipes
    RouterModule.forChild(ROUTER_CONFIG),
    BrowserModule
  ]
})
export default class DirectoryModule {
  static routes = ROUTER_CONFIG;
}

