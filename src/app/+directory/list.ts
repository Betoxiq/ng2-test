import { Component,OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Asset } from '../../assets/model';
import {AppState} from '../../assets/state';
import { Store } from '@ngrx/store';

@Component({
  template: `
    <h1>List</h1>
     <table>
      <thead>
        <tr>
        <th></th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        <tr *ngFor="let asset of assets | async; let i=index ">
        <td> {{ i+1 }} </td>
          <td>{{ asset.name }}</td>
          <td>
            <a routerLink="detail/{{ asset._id }}">
              {{ asset._id }}
              </a>
          </td>

        </tr>
      </tbody>
    </table>
  `
})
export class List implements OnInit {

  assets : Observable<Asset[]>;
  constructor(private store: Store<AppState>) {
    console.log('list ctor called');
  }

  ngOnInit(){
    this.assets = this.store.select(state=>state.assets);
  }

}
/**
 *
 *
 */