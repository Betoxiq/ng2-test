import {Injectable } from '@angular/core';
import { Router, Resolve, ResolveData, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Asset } from '../../assets/model';
import { AppState } from '../../assets/state';
import { Store } from '@ngrx/store';



@Injectable()
export class DetailResolver implements Resolve<any> {
    result: Observable<any>;

    constructor(private store: Store<AppState>) {
        console.log('detail resolver');

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<number>  {
        console.log('testing detail resolver ');
        console.log(route.params["id"]);
        this.store.select()
        //this.result = this.swapi.getPerson(route.params["id"]);
//        this.result = Observable.of(route.params["id"]*1);
        return this.result;
    }
}