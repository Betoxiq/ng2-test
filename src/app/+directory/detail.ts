import { Component } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Component({
  providers:[],
  template: `
    <h1>Detail = {{ (person | async).name  }}</h1>
  `
})
export class Detail {
  person: Observable<any>;
  constructor(route: ActivatedRoute) {
    this.person = route.data.map( result=> result["detail"] ) ;
  }

}
