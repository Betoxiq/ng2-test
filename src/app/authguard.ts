import { Injectable } from '@angular/core';
import { CanLoad, Route, Router } from '@angular/router';
import { AuthService } from './authservice';

@Injectable()
export class AuthGuard implements CanLoad{

    constructor(private authService: AuthService, private router: Router) {
        console.log('authguard ctor');
    }

    canLoad(route:Route){
        console.log('authguard canload');
        return this.authService.isLoggedIn();
    }
}