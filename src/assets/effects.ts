import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/switchMapTo';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/observable/of';
import { Injectable } from '@angular/core';
import { Effect, StateUpdates, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Database } from '@ngrx/db';

import { AssetState } from './state';
//import { GoogleAssetsService } from '../services/google-assets';
import { AssetActions } from './actions';
import { Asset } from './model';


@Injectable()
export class AssetEffects {
  constructor(
    private updates$: StateUpdates<AssetState>,
    //private googleAssets: GoogleAssetsService,
    private db: Database,
    private assetActions: AssetActions
  ) { }

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application. StateUpdates is an observable of the latest state and
 * dispatched action. The `toPayload` helper function returns just
 * the payload of the currently dispatched action, useful in
 * instances where the current state is not necessary.
 *
 * If you are unfamiliar with the operators being used in these examples, please
 * check out the sources below:
 *
 * Official Docs: http://reactivex.io/rxjs/manual/overview.html#categories-of-operators
 * RxJS 5 Operators By Example: https://gist.github.com/btroncone/d6cf141d6f2c00dc6b35
 */
  @Effect() openDB$ = this.db.open('asset_app').filter(() => false);


  @Effect() loadCollectionOnInit$ = Observable.of(this.assetActions.loadCollection());


  @Effect() loadCollection$ = this.updates$
    .whenAction(AssetActions.LOAD_COLLECTION)
    .switchMapTo(this.db.query('assets').toArray())
    .map((assets: Asset[]) => this.assetActions.loadCollectionSuccess(assets));


// //   @Effect() search$ = this.updates$
// //     .whenAction(AssetActions.SEARCH)
// //     .map<string>(toPayload)
// //     .filter(query => query !== '')
// //     .switchMap(query => this.googleAssets.searchAssets(query)
// //       .map(assets => this.assetActions.searchComplete(assets))
// //       .catch(() => Observable.of(this.assetActions.searchComplete([])))
// //     );


//   @Effect() clearSearch$ = this.updates$
//     .whenAction(AssetActions.SEARCH)
//     .map<string>(toPayload)
//     .filter(query => query === '')
//     .mapTo(this.assetActions.searchComplete([]));


  @Effect() addAssetToCollection$ = this.updates$
    .whenAction(AssetActions.ADD_TO_COLLECTION)
    .map<Asset>(toPayload)
    .mergeMap(asset => this.db.insert('assets', [ asset ])
      .mapTo(this.assetActions.addToCollectionSuccess(asset))
      .catch(() => Observable.of(
        this.assetActions.addToCollectionFail(asset)
      ))
    );


  @Effect() removeAssetFromCollection$ = this.updates$
    .whenAction(AssetActions.REMOVE_FROM_COLLECTION)
    .map<Asset>(toPayload)
    .mergeMap(asset => this.db.executeWrite('assets', 'delete', [ asset._id ])
      .mapTo(this.assetActions.removeFromCollectionSuccess(asset))
      .catch(() => Observable.of(
        this.assetActions.removeFromCollectionFail(asset)
      ))
    );
}