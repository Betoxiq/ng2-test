import { ActionReducer, Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import '@ngrx/core/add/operator/select';

import { AssetActions } from './actions';
import { Asset } from './model';
import { AssetState } from './state';


const initialState: AssetState = {
    ids: [],
    entities: {}
};
export default function (state = initialState, action: Action): AssetState {
    switch (action.type) {
        case AssetActions.LOAD_COLLECTION_SUCCESS: {
            const assets: Asset[] = action.payload;
            const newAssets = assets.filter(asset => !state.entities[asset._id]);

            const newAssetIds = newAssets.map(asset => asset._id);
            const newAssetEntities = newAssets.reduce((entities: { [id: string]: Asset }, asset: Asset) => {
                return Object.assign(entities, {
                    [asset._id]: asset
                });
            }, {});

            return {
                ids: [...state.ids, ...newAssetIds],
                entities: Object.assign({}, state.entities, newAssetEntities)
            };
        }

        default:
            return state;
    }
}

export function getAssetEntities() {
    return (state$: Observable<AssetState>) => state$
        .select(s => s.entities);
};



