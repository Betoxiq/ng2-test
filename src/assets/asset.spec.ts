import 'rxjs/add/operator/let';
import { of } from 'rxjs/observable/of';
import { AssetActions } from './actions';
import {AssetsReducer } from './reducer';
import { Asset } from './model';

export const TestAsset: Asset = {
  _id: '123',
  name:"ABC"
};
describe('Assets', function () {
  const assetActions = new AssetActions();
  describe('Reducer', function () {
    it('should have an empty initial state', function () {
      const initialState = AssetsReducer([], { type: 'test-action' });
      expect(initialState).toEqual([]);
    });

    it('should add a asset to the entities table and its ID to the IDs list when loaded', function () {
      const action = assetActions.loadAsset(TestAsset);
      const state = AssetsReducer([], action);
      expect(state[0]._id).toBe(TestAsset._id);
    });
  });

});