import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/rx';

import { Asset } from './model';

let PouchDB = require('pouchdb');

@Injectable()
export class AssetService {
    private db;

    /**
     *
     */
    constructor() {

    }
    initDB(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            try {
                this.db = new PouchDB("test");
                if (typeof window != "undefined") {
                    console.log("setting pouchdb on window");
                    window["PouchDB"] = PouchDB;
                }
                resolve(this.db);
            } catch (error) {
                reject(error);
            }
        });
    }

    add(Asset: Asset): Promise<any> {
        return this.db.post(Asset);
    }

    update(Asset: Asset): Promise<any> {
        return this.db.put(Asset);
    }

    delete(Asset: Asset): Promise<any> {
        return this.db.remove(Asset);
    }

    getById(id:string){
        return this.db.get(id);
    }

    getAll(): Observable<any> {
        return Observable.fromPromise(
            this.initDB()
                .then(() => {
                    return this.db.allDocs({ include_docs: true });
                })
                .then(docs => {

                    // Each row has a .doc object and we just want to send an
                    // array of Asset objects back to the calling code,
                    // so let's map the array to contain just the .doc objects.

                    return docs.rows.map(row => {
                        // Convert string to date, doesn't happen automatically.
                        row.doc.date = new Date(row.doc.date);
                        return row.doc;
                    });
                }));
    }

    getChanges(): Observable<any> {
        return Observable.create(observer => {

            // Listen for changes on the database.
            this.db.changes({ live: true, since: 'now', include_docs: true })
                .on('change', change => {
                    // Convert string to date, doesn't happen automatically.
                    change.doc.date = new Date(change.doc.date);
                    observer.next(change.doc);
                });
        });
    }
}