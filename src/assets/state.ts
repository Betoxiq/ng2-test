import {Asset} from './model';

export interface AssetState {
    ids: string[];
    entities: { [id: string]: Asset };
}