import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

import { Asset } from './model';

@Injectable()
export class AssetActions {

    static LOAD_COLLECTION = '[Asset] Load Collection';
    loadCollection(): Action {
        return {
            type: AssetActions.LOAD_COLLECTION
        };
    }

    static LOAD_COLLECTION_SUCCESS = '[Asset] Load Collection Success';
    loadCollectionSuccess(assets: Asset[]): Action {
        return {
            type: AssetActions.LOAD_COLLECTION_SUCCESS,
            payload: assets
        };
    }

    static ADD_TO_COLLECTION = '[Asset] Add to Collection';
    addToCollection(asset: Asset): Action {
        return {
            type: AssetActions.ADD_TO_COLLECTION,
            payload: asset
        };
    }

    static ADD_TO_COLLECTION_SUCCESS = '[Asset] Add to Collection Success';
    addToCollectionSuccess(asset: Asset): Action {
        return {
            type: AssetActions.ADD_TO_COLLECTION_SUCCESS,
            payload: asset
        };
    }

    static ADD_TO_COLLECTION_FAIL = '[Asset] Add to Collection Fail';
    addToCollectionFail(asset: Asset): Action {
        return {
            type: AssetActions.ADD_TO_COLLECTION_FAIL,
            payload: asset
        };
    }


    static LOAD_ASSET = '[Asset] Load Asset';
    loadAsset(asset: Asset): Action {
        return {
            type: AssetActions.LOAD_ASSET,
            payload: asset
        };
    }

    static UPDATE_ASSET = 'UPDATE_ASSET';
    updateAsset(asset: Asset): Action {
        return {
            type: AssetActions.UPDATE_ASSET,
            payload: asset
        }
    }

    static DELETE_ASSET = 'DELETE_ASSET';
    deleteAsset(asset: Asset): Action {
        return {
            type: AssetActions.DELETE_ASSET,
            payload: asset
        }
    }

    static LOAD_ASSETS_SUCCESS = 'LOAD_ASSETS_SUCCESS';
    loadAssetsSuccess(assets: Asset[]): Action {
        return {
            type: AssetActions.LOAD_ASSETS_SUCCESS,
            payload: assets
        }
    }

    static ADD_UPDATE_ASSET_SUCCESS = 'ADD_UPDATE_ASSET_SUCCESS';
    addUpdateAssetSuccess(asset: Asset): Action {
        return {
            type: AssetActions.ADD_UPDATE_ASSET_SUCCESS,
            payload: asset
        }
    }

    static DELETE_ASSET_SUCCESS = 'DELETE_ASSET_SUCCESS';
    deleteAssetSuccess(id: string): Action {
        return {
            type: AssetActions.DELETE_ASSET_SUCCESS,
            payload: id
        }
    }
}