import {DBSchema} from '@ngrx/db';

const schema: DBSchema = {
    version: 1,
    name: 'swapi_app',
    stores:{
        people:{
            autoIncrement:true,
            primaryKey: 'id'
        }
    }
}

export default schema;