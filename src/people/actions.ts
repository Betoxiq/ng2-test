import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {Person} from './model';

@Injectable()
export class PersonActions {
    static SEARCH = '[Person] Search';
    search(query: string): Action {
        return {
            type: PersonActions.SEARCH,
            payload: query
        }
    }

    static SEARCH_COMPLETE = '[Person] Search Complete';
    searchComplete(results: Person[]): Action {
        return {
            type: PersonActions.SEARCH_COMPLETE,
            payload: results
        }
    }


    static ADD_TO_COLLECTION = '[Person] Add to Collection';
    addToCollection(person: Person): Action {
        return {
            type: PersonActions.ADD_TO_COLLECTION,
            payload: person
        }
    }

    static ADD_TO_COLLECTION_SUCCESS = '[Person] Add to Collection Success';
    addToCollectionSuccess(person: Person): Action {
        return {
            type: PersonActions.ADD_TO_COLLECTION_SUCCESS,
            payload: person
        }
    }


    static ADD_TO_COLLECTION_FAIL = '[Person] Add to Collection Fail';
    addToCollectionFail(person: Person): Action {
        return {
            type: PersonActions.ADD_TO_COLLECTION_FAIL,
            payload: person
        }
    }


    static REMOVE_FROM_COLLECTION = '[Person] Remove from Collection';
    removeFromCollection( person: Person ): Action {
        return {
            type: PersonActions.REMOVE_FROM_COLLECTION,
            payload: person
        }
    }


    static REMOVE_FROM_COLLECTION_SUCCESS = '[Person] Remove from Collection Success';
    removeFromCollectionSuccess( person: Person ): Action {
        return {
            type: PersonActions.REMOVE_FROM_COLLECTION_SUCCESS,
            payload: person
        }
    }


    static REMOVE_FROM_COLLECTION_FAIL = '[Person] Remove from Collection Fail';
    removeFromCollectionFail( person: Person ): Action {
        return {
            type: PersonActions.REMOVE_FROM_COLLECTION_FAIL,
            payload: person
        }
    }


    static LOAD_COLLECTION = '[Person] Load Collection';
    loadCollection(  ): Action {
        return {
            type: PersonActions.LOAD_COLLECTION
        }
    }


    static LOAD_COLLECTION_SUCCESS = '[Person] Load Collection Success';
    loadCollectionSuccess( people: Person[] ): Action {
        return {
            type: PersonActions.LOAD_COLLECTION_SUCCESS,
            payload: people
        }
    }

    static LOAD_COLLECTION_FAIL = '[Person] Load Collection Fail';
    loadCollectionFail( people: Person[] ): Action {
        return {
            type: PersonActions.LOAD_COLLECTION_FAIL,
            payload: people
        }
    }

    static LOAD_PERSON = '[Person] Load Person';
    loadPerson( person: Person ): Action {
        return {
            type: PersonActions.LOAD_PERSON,
            payload: person
        }
    }


}