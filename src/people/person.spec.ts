import 'rxjs/add/operator/let';
import { of } from 'rxjs/observable/of';
import { PersonActions } from './actions';
import personReducer, * as fromPersons from './reducer';
import { TestPerson } from './fixtures';

describe('Persons', function () {
  const personActions = new PersonActions();
  describe('Reducer', function () {
    it('should have an empty initial state', function () {
      const initialState = personReducer(undefined, { type: 'test-action' });
      expect(initialState.ids).toEqual([]);
      expect(initialState.entities).toEqual({});
    });

    it('should add a person to the entities table and its ID to the IDs list when loaded', function () {
      const action = personActions.loadPerson(TestPerson);
      const state = personReducer(undefined, action);
      expect(state.ids).toEqual([TestPerson.id]);
      expect(state.entities[TestPerson.id]).toBe(TestPerson);
    });
  });

  describe('Selectors', function () {
    describe('getPersonEntities', function () {
      it('should get the entities table out of the people state', function () {
        const state = personReducer(undefined, { type: 'test-action' });

        of(state).let(fromPersons.getPersonEntities()).subscribe(entities => {
          expect(entities).toBe(state.entities);
        });
      });
    });

 describe('getPerson', function() {
      it('should get a selected person out of the people state', function() {
        const state: fromPersons.PersonState = {
          entities: {
            [TestPerson.id]: TestPerson
          },
          ids: [ TestPerson.id ]
        };

        of(state).let(fromPersons.getPerson(TestPerson.id)).subscribe(person => {
          expect(person).toBe(TestPerson);
        });
      });
    });

    describe('getPeople', function() {
      it('should return all of the people in an array for a given list of ids', function() {
        const state: fromPersons.PersonState = {
          entities: {
            [TestPerson.id]: TestPerson
          },
          ids: [ TestPerson.id ]
        };

        of(state).let(fromPersons.getPeople([ TestPerson.id ])).subscribe(people => {
          expect(people).toEqual([ TestPerson ]);
        });
      });
    });
  });
});