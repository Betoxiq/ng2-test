import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/switchMapTo';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/observable/of';
import { Injectable } from '@angular/core';
import { Effect, StateUpdates, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Database } from '@ngrx/db';

import { PersonState } from './reducer';
import { GoogleBooksService } from './bookservice';
import { PersonActions } from './actions';
import { Person } from './model';


@Injectable()
export class PersonEffects {
  constructor(
    private updates$: StateUpdates<PersonState>,
    private googleBooks: GoogleBooksService,
    private db: Database,
    private personActions: PersonActions
  ) { }

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application. StateUpdates is an observable of the latest state and
 * dispatched action. The `toPayload` helper function returns just
 * the payload of the currently dispatched action, useful in
 * instances where the current state is not necessary.
 *
 * If you are unfamiliar with the operators being used in these examples, please
 * check out the sources below:
 *
 * Official Docs: http://reactivex.io/rxjs/manual/overview.html#categories-of-operators
 * RxJS 5 Operators By Example: https://gist.github.com/btroncone/d6cf141d6f2c00dc6b35
 */
  @Effect() openDB$ = this.db.open('person_app').filter(() => false);


  @Effect() loadCollectionOnInit$ = Observable.of(this.personActions.loadCollection());


  @Effect() loadCollection$ = this.updates$
    .whenAction(PersonActions.LOAD_COLLECTION)
    .switchMapTo(this.db.query('books').toArray())
    .map((books: Person[]) => this.personActions.loadCollectionSuccess(books));


  @Effect() search$ = this.updates$
    .whenAction(PersonActions.SEARCH)
    .map<string>(toPayload)
    .filter(query => query !== '')
    .switchMap(query => this.googleBooks.searchBooks(query)
      .map(people => this.personActions.searchComplete(people))
      .catch(() => Observable.of(this.personActions.searchComplete([])))
    );


  @Effect() clearSearch$ = this.updates$
    .whenAction(PersonActions.SEARCH)
    .map<string>(toPayload)
    .filter(query => query === '')
    .mapTo(this.personActions.searchComplete([]));


  @Effect() addBookToCollection$ = this.updates$
    .whenAction(PersonActions.ADD_TO_COLLECTION)
    .map<Person>(toPayload)
    .mergeMap(person => this.db.insert('people', [ person ])
      .mapTo(this.personActions.addToCollectionSuccess(person))
      .catch(() => Observable.of(
        this.personActions.addToCollectionFail(person)
      ))
    );


  @Effect() removeBookFromCollection$ = this.updates$
    .whenAction(PersonActions.REMOVE_FROM_COLLECTION)
    .map<Person>(toPayload)
    .mergeMap(person => this.db.executeWrite('people', 'delete', [ person.id ])
      .mapTo(this.personActions.removeFromCollectionSuccess(person))
      .catch(() => Observable.of(
        this.personActions.removeFromCollectionFail(person)
      ))
    );
}