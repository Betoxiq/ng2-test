import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { Person } from './model';
import { PersonActions } from './actions';


export interface PersonState {
    ids: string[];
    entities: { [id: string]: Person };
}

const initialState: PersonState = {
    ids: [],
    entities: {}
}

export default function (state = initialState, action: Action): PersonState {
    switch (action.type) {
        case PersonActions.SEARCH_COMPLETE:
        case PersonActions.LOAD_COLLECTION_SUCCESS: {
            const people: Person[] = action.payload;
            const newPeople: people.filter( person => !state.entities[person.id]);
            const newPersonIds = newPeople.map(person => person.id);
            const newPersonEntities = newPeople.reduce((entities: { [id: string]: Person }, person: Person) => {
                return Object.assign(entities, {
                    [person.id]: person
                });
            }, {});
        }
        case PersonActions.LOAD_PERSON: {
            const person: Person = action.payload;
            if (state.ids.includes(person.id)) {
                return state;
            }
            return {
                ids: [...state.ids, person.id],
                entities: Object.assign({}, state.entities, {
                    [person.id]: person
                })
            }
        }

        default: {
            return state;
        }
    }

}

export function getPersonEntities() {
  return (state$: Observable<PersonState>) => state$
    .select(s => s.entities);
};

export function getPerson(id: string) {
  return (state$: Observable<PersonState>) => state$
    .select(s => s.entities[id]);
}

export function getPeople(personIds: string[]) {
  return (state$: Observable<PersonState>) => state$
    .let(getPersonEntities())
    .map(entities => personIds.map(id => entities[id]));
}

export function hasPerson(id: string) {
  return (state$: Observable<PersonState>) => state$
    .select(s => s.ids.includes(id));
}
